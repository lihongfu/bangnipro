<?php
return [
    'file_domain' => \think\facade\Env::get('project.file_domain', ''),
    'sms' => \think\facade\Env::get('project.sms', true),
    'version' => \think\facade\Env::get('project.version', '2.8.9.20230426'),
    'front_version' => \think\facade\Env::get('project.version', '2.8.9.20230426'),
];