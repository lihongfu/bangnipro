<?php

use think\facade\Route;

Route::group('api/v1/', function () {
    Route::bind('api');
    //菜单管理、权限管理
    Route::group('index', function () {
        Route::get('/getPlatformInfo', '/getPlatformInfo');
    })->prefix('v1.Index');
    Route::group('ad_content', function () {
        Route::get('/lists', '/lists');
    })->prefix('v1.AdContent');

    Route::group('goods', function () {
        Route::get('/getSearchPage', '/getSearchPage');
        Route::post('/clearSearch', '/clearSearch');
        Route::get('/getGoodsList', '/getGoodsList');
        Route::get('/getGoodsDetail', '/getGoodsDetail');
    })->prefix('v1.Goods');

    Route::group('account', function () {
        Route::post('/silentLogin', '/silentLogin');
        Route::post('/authLogin', '/authLogin');
    })->prefix('v1.Account');

    Route::group('goods_category', function () {
        Route::get('/lists', '/lists');
    })->prefix('v1.GoodsCategory');

    Route::group('article', function () {
        Route::get('/category', '/category');
        Route::get('/lists', '/lists');
        Route::get('/detail', '/detail');
    })->prefix('v1.Article');

    Route::group('user', function () {
        Route::get('/center', '/center');
        Route::get('/accountLog', '/accountLog');
        Route::get('/getDefaultAddress', '/getDefaultAddress');
    })->prefix('v1.User');

    Route::group('recharge', function () {
        Route::get('/rechargeTemplate', '/rechargeTemplate');
        Route::post('/recharge', '/recharge');
    })->prefix('v1.Recharge');

    Route::group('user_address', function () {
        Route::get('/lists', '/lists');
        Route::get('/detail', '/detail');
        Route::post('/add', '/add');
        Route::post('/update', '/update');
        Route::post('/setDefault', '/setDefault');
        Route::get('/getDefault', '/getDefault');
    })->prefix('v1.UserAddress');

    Route::group('user_guest', function () {
        Route::get('/lists', '/lists');
        Route::get('/detail', '/detail');
        Route::post('/add', '/add');
        Route::post('/update', '/update');
        Route::post('/setDefault', '/setDefault');
        Route::get('/getDefault', '/getDefault');
    })->prefix('v1.UserGuest');

    Route::group('order', function () {
        Route::post('/buy', '/buy');
        Route::get('/lists', '/lists');
        Route::get('/detail', '/detail');
        Route::post('/update', '/update');
    })->prefix('v1.Order');

    Route::group('payment', function () {
        Route::get('/payway', '/payway');
        Route::post('/prepay', '/prepay');
        Route::post('/notifyMnp', '/notifyMnp')->name('notifyMnp');
        Route::post('/notifyOa', '/notifyOa')->name('notifyOa');
        Route::post('/notifyApp', '/notifyApp')->name('notifyApp');
    })->prefix('v1.Payment');

    Route::group('selffetch_shop', function () {
        Route::get('/getMapKey', '/getMapKey');
    })->prefix('v1.SelffetchShop');

    Route::group('file', function () {
        Route::post('/formimage', '/formimage');
    })->prefix('v1.File');

})->middleware(\app\api\http\middleware\Login::class);
