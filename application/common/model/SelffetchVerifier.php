<?php
// +----------------------------------------------------------------------
// |
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | 商业版本务必购买商业授权，以免引起法律纠纷
// | 禁止对系统程序代码以任何目的，任何形式的再发布



// +----------------------------------------------------------------------

// +----------------------------------------------------------------------

namespace app\common\model;


use think\Model;

class SelffetchVerifier extends Model
{
    //核销员状态
    const CLOSE = 0;//停用
    const OPEN = 1;//启用

    /**
     * @notes 核销员状态
     * @param bool $status
     * @return string|string[]
     * @author ljj
     * @date 2021/8/16 3:27 下午
     */
    public static function getVerifierStatus($status = true)
    {
        $desc = [
            self::CLOSE => '停用',
            self::OPEN => '启用',
        ];
        if ($status === true) {
            return $desc;
        }
        return $desc[$status] ?? '未知';
    }

    /**
     * @notes 核销员状态获取器
     * @param $value
     * @param $data
     * @return string|string[]
     * @author ljj
     * @date 2021/8/16 3:35 下午
     */
    public function getStatusDescAttr($value,$data)
    {
        return self::getVerifierStatus($data['status']);
    }

    /**
     * @notes 创建时间获取器
     * @param $value
     * @param $data
     * @return false|string
     * @author ljj
     * @date 2021/8/16 3:35 下午
     */
    public function getCreateTimeAttr($value,$data)
    {
        return date('Y-m-d H:i:s',$value);
    }
}