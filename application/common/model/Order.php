<?php
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | author:
// +----------------------------------------------------------------------

namespace app\common\model;

use think\Db;
use think\Model;

class Order extends Model
{
    protected $name = 'order';

    //订单类型
    const NORMAL_ORDER = 0;//普通订单
    const SECKILL_ORDER = 1;//秒杀订单
    const TEAM_ORDER = 2;//拼团订单
    const BARGAIN_ORDER = 3;//砍价订单

    //订单状态
    const STATUS_WAIT_PAY = 0;       //待付款
    const STATUS_WAIT_DELIVERY = 1;  //待发货
    const STATUS_WAIT_RECEIVE = 2;   //待收货
    const STATUS_FINISH = 3;         //已完成
    const STATUS_CLOSE = 4;          //已关闭

    //配送方式
    const DELIVERY_STATUS_EXPRESS = 1;//快递配送
    const DELIVERY_STATUS_SELF = 2;//上门自提

    //核销状态
    const NOT_WRITTEN_OFF = 0;//待核销
    const WRITTEN_OFF = 1;//已核销

    //订单状态
    public static function getOrderStatus($status = true)
    {
        $desc = [
            self::STATUS_WAIT_PAY => '待付款',
            self::STATUS_WAIT_DELIVERY => '服务中',
            self::STATUS_WAIT_RECEIVE => '未定义状态',
            self::STATUS_FINISH => '已完成',
            self::STATUS_CLOSE => '已关闭',
        ];
        if ($status === true) {
            return $desc;
        }
        return $desc[$status] ?? '未知';
    }


    //订单类型
    public static function getOrderType($type)
    {
        $desc = [
            self::NORMAL_ORDER => '普通订单',
            self::SECKILL_ORDER => '秒杀订单',
            self::TEAM_ORDER => '拼团订单',
            self::BARGAIN_ORDER => '砍价订单',
        ];

        if ($type === true){
            return $desc;
        }
        return $desc[$type] ?? '未知';
    }

    //配送方式
    public static function getDeliveryType($type)
    {
        $desc = [
            self::DELIVERY_STATUS_EXPRESS => '上门服务',
            self::DELIVERY_STATUS_SELF => '到店服务'
        ];

        if ($type === true){
            return $desc;
        }
        return $desc[$type] ?? '未知';
    }

    /**
     * @notes 核销状态
     * @param $type
     * @return string|string[]
     * @author ljj
     * @date 2021/8/16 7:31 下午
     */
    public static function getVerificationStatus($type)
    {
        $desc = [
            self::NOT_WRITTEN_OFF => '待核销',
            self::WRITTEN_OFF => '已核销',
        ];

        if ($type === true){
            return $desc;
        }
        return $desc[$type] ?? '未知';
    }


    //下单时间
    public function getCreateTimeAttr($value, $data)
    {
        return date('Y-m-d H:i:s', $value);
    }

    //付款时间
    public function getPayTimeAttr($value, $data)
    {
        if ($value) {
            return date('Y-m-d H:i:s', $value);
        }
        $value = '未支付';
        return $value;
    }

    // 发货时间
    public function getShippingTimeAttr($value, $data)
    {
        if ($value) {
            return date('Y-m-d H:i:s', $value);
        }
        return $value;
    }

    //收货时间
    public function getTakeTimeAttr($value, $data)
    {
        if ($value) {
            return date('Y-m-d H:i:s', $value);
        }
        return $value;
    }

    //取消时间
    public function getCancelTimeAttr($value, $data)
    {
        if ($value) {
            return date('Y-m-d H:i:s', $value);
        }
        return $value;
    }

    //订单类型
    public function getOrderTypeTextAttr($value, $data)
    {
        return self::getOrderType($data['order_type']);
    }

    //订单状态
    public function getOrderStatusTextAttr($value, $data)
    {
        return self::getOrderStatus($data['order_status']);
    }

    //订单支付方式
    public function getPayWayTextAttr($value, $data)
    {
        return Pay::getPayWay($data['pay_way']);
    }

    //订单支付状态
    public function getPayStatusTextAttr($value, $data)
    {
        return Pay::getPayStatus($data['pay_status']);
    }

    //订单来源
    public function getOrderSourceTextAttr($value, $data)
    {
        return Client_::getClient($data['order_source']);
    }

    //订单商品数量
    public function getGoodsCountAttr($value, $data)
    {
        return count($this->order_goods);
    }


    //订单关联商品
    public function orderGoods()
    {
        return $this->hasMany('order_goods', 'order_id', 'id');
    }

    //订单用户
    public function user()
    {
        return $this->hasOne('user', 'id', 'user_id')
            ->field('id,sn,nickname,avatar,level,mobile,sex,create_time');
    }

    //收货地址
    public function getDeliveryAddressAttr($value, $data)
    {
        $region = Db::name('dev_region')
            ->where('id', 'IN', [$data['province'], $data['city'], $data['district']])
            ->order('level asc')
            ->column('name');

        $region_desc = implode('', $region);
        return $region_desc . $data['address'];
    }

    /**
     * @notes 核销状态
     * @param $value
     * @param $data
     * @return string|string[]
     * @author ljj
     * @date 2021/8/16 7:32 下午
     */
    public function getVerificationStatusTextAttr($value, $data)
    {
        return self::getVerificationStatus($data['verification_status']);
    }
}