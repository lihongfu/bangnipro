<?php
// +----------------------------------------------------------------------
// |
// +----------------------------------------------------------------------
// | 欢迎阅读学习系统程序代码，建议反馈是我们前进的动力
// | 开源版本可自由商用，可去除界面版权logo
// | 商业版本务必购买商业授权，以免引起法律纠纷
// | 禁止对系统程序代码以任何目的，任何形式的再发布



// +----------------------------------------------------------------------

// +----------------------------------------------------------------------

namespace app\common\model;


use think\Model;

class Express extends Model
{


    const ZHONGTONG  = 'zhongtong';
    const SHENTONG   = 'shentong';

    /**
     * @notes 获取快递100
     * @param bool $from
     * @return mixed|string
     * @author cjhao
     * @date 2022/9/29 10:46
     */
    public static function getkuaidi100code($from = true)
    {
        $desc = [
//            self::ZHONGTONG         => 'ztoOpen',
            self::SHENTONG          => '44',
        ];
        return $desc[$from] ?? '';
    }
}