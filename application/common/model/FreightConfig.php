<?php
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | author:
// +----------------------------------------------------------------------

namespace app\common\model;

use think\Model;

class FreightConfig extends Model
{
    protected $name = 'freight_config';

    protected $autoWriteTimestamp = true;

}