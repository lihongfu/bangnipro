<?php
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | author: 
// +----------------------------------------------------------------------
namespace app\admin\validate;


use think\Validate;

class GoodsOneSpec extends Validate
{
    protected $rule = [
        'one_market_price' => 'egt:0',
        'one_price'        => 'require|egt:0.01',
        'one_cost_price'   => 'require|egt:0.01',
        'one_stock'        => 'require|integer|gt:0',
        'one_duration'       => 'require|integer|gt:0',
        'one_overview'       => 'require',
    ];

    protected $message = [
        'one_weight.require'        => '请输入重量',
        'one_weight.egt'             => '重量必须为大于或等于0',
//        'one_market_price.require'  => '请输入市场价',
        'one_market_price.egt'      => '市场价不能小于0',
        'one_price.require'         => '请输入价格',
        'one_price.egt'              => '价格必须大于或等于0.01',
        'one_cost_price.require'    => '请输入成本价',
        'one_cost_price.egt'         => '成本价必须大于或等于0.01',
        'one_stock.require'         => '请输入库存',
        'one_stock.integer'         => '库存必须为整型',
        'one_stock.gt'              => '库存必须大于零',
        'one_duration.require'         => '请输入服务时长',
        'one_duration.integer'         => '服务时长必须为整型',
        'one_duration.gt'              => '服务时长必须大于零',
        'one_overview.require'        => '请输入服务概述',
    ];


}