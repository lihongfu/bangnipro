<?php
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | author: 
// +----------------------------------------------------------------------
namespace app\admin\validate;


use think\Validate;

class GoodsMoreSpecLists extends Validate
{
    protected $rule = [
        'market_price'  => 'egt:0',
        'price'         => 'require|egt:0.01',
        'cost_price'    => 'require|egt:0.01',
        'stock'         => 'require|integer|egt:0',
        'duration'        => 'require|integer|gt:0',
        'overview'        => 'require',
    ];

    protected $message = [
        'volume.require'        => '请输入体积',
        'volume.egt'             => '体积必须大于或等于0',
        'weight.require'        => '请输入重量',
        'weight.egt'             => '重量必须大于或等于0',
//        'market_price.require'  => '请输入市场价',
        'market_price.egt'      => '市场价不能小于0',
        'price.require'         => '请输入价格',
        'price.egt'              => '价格必须大于或等于0.01',
        'cost_price.require'    => '请输入成本价',
        'cost_price.egt'         => '成本价必须大于或等于0.01',
        'stock.require'         => '请输入库存',
        'stock.integer'         => '库存必须为整数',
        'stock.egt'              => '库存必须大于或等于0',
        'duration.require'         => '请输入服务时长',
        'duration.integer'         => '服务时长必须为整型',
        'duration.gt'              => '服务时长必须大于零',
        'overview.require'        => '请输入服务概述',
    ];


}