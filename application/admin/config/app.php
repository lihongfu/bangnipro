<?php
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | author: 
// +----------------------------------------------------------------------

use think\facade\Env;

return [

    //默认错误跳转对应的模板文件
    'dispatch_error_tmpl' => '../application/admin/view/dispatch/error.html',
    //默认成功跳转对应的模板文件
    'dispatch_success_tmpl' => '../application/admin/view/dispatch/success.html',
];