<?php
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | author:
// +----------------------------------------------------------------------


namespace app\api\validate;


use think\Validate;

class UserGuest extends Validate
{

    protected $rule = [
        'id'            => 'require|integer',
        'name'   => 'require',
        'idcard'     => 'require|idCard',
    ];

    protected $message = [
        'id.require'            => 'id不能为空',
        'id.integer'            => 'id参数错误',
        'name.require'            => '请输入患者姓名',
        'idcard.require'     => '请输入患者身份证号',
        'idcard.idCard'      => '身份证号不正确',
    ];

    /**
     * 添加
     */
    public function sceneAdd()
    {
        return $this->remove('id');
    }

    /**
     * 编辑
     */
    public function sceneEdit()
    {

    }

    /**
     * 删除
     */
    public function sceneDel()
    {
        return $this->only(['id']);
    }

    /**
     * 获取一条地址
     */
    public function sceneOne()
    {
        return $this->only(['id']);
    }

    /**
     * 设置默认地址
     */
    public function sceneSet()
    {
        return $this->only(['id']);
    }
}