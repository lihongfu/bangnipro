<?php
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | author: 
// +----------------------------------------------------------------------


namespace app\api\logic;


use app\common\server\AreaServer;
use think\Db;
use think\Exception;

class UserGuestLogic
{
    /**
     * 获取用户地址信息
     * @param $user_id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function infoUserGuest($user_id){
        $info = Db::name('user_guest')
            ->where(['user_id'=>$user_id,'del'=>0])
            ->field('id,name,idcard,sex,age,is_certificates,is_realname,is_default')
            ->select();
        foreach ($info as &$item) {
            $item['sexText'] = getSexText($item['sex']);
            $item['age'] = get_age($item['idcard']);
        }
        return $info;
    }

    /**
     * 获取一条地址信息
     * @param $user_id
     * @param $get
     * @return array|\PDOStatement|string|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getOneGuest($user_id,$get) {
        $info = Db::name('user_guest')
            ->where(['id'=>$get['id'],'user_id'=>$user_id,'del'=>0])
            ->field('id,name,idcard,sex,age,is_certificates,is_realname,is_default')
            ->find();

        $info['sexText'] = getSexText($info['sex']);
        $info['age'] = get_age($info['idcard']);
        return $info;
    }

    /**
     * 获取默认地址
     * @param $user_id
     * @return array|\PDOStatement|string|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getDefaultGuest($user_id){
        $info = Db::name('user_guest')
            ->where(['is_default'=>1,'user_id'=>$user_id,'del'=>0])
            ->field('id,name,idcard,sex,age,is_certificates,is_realname,is_default')
            ->find();

        if (!$info){
            return [];
        }

        $info['sexText'] = getSexText($info['sex']);

        return $info;
    }

    /**
     * 设置默认地址
     * @param $user_id
     * @param $post
     * @return int|string
     */
    public static function setDefaultGuest($user_id,$post){

        try {
            Db::startTrans();

            Db::name('user_guest')
                ->where(['del'=> 0 ,'user_id'=>$user_id])
                ->update(['is_default'=>0]);

            $result = Db::name('user_guest')
                ->where(['id'=>$post['id'],'del'=> 0 ,'user_id'=>$user_id])
                ->update(['is_default'=>1]);

            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            return false;
        }

        return $result;
    }

    public static function idcardIsUnique($user_id,$post)
    {
        $query = Db::name('user_guest')
            ->where(['del'=> 0 ,'user_id'=>$user_id,'idcard'=>$post['idcard']]);

        if(!empty($post['id'])){
            $query->where('id','<>',$post['id']);
        }

        $count = $query->count();

        return $count?false:true;
    }

    /**
     * 添加收货地址
     * @param $user_id
     * @param $post
     * @return int|string
     */
    public static function addUserGuest($user_id,$post) {
        try {
            Db::startTrans();

            if (isset($post['is_default']) && $post['is_default'] == 1){
                Db::name('user_guest')
                    ->where(['del'=> 0 ,'user_id'=>$user_id])
                    ->update(['is_default'=>0]);
            }else{
                $is_first = Db::name('user_guest')
                    ->where(['del'=> 0 ,'user_id'=>$user_id])
                    ->find();
                if (empty($is_first)){
                    $post['is_default'] = 1;
                }
            }

            //通过身份证号查询出性别与生日
            $age = get_age($post['idcard']);
            $sex = get_sex($post['idcard']);

            $data = [
                'user_id'       =>  $user_id,
                'name'       =>  $post['name'],
                'idcard'     =>  $post['idcard'],
                'age'       =>  $age,
                'sex'       =>  $sex,
                'is_default'    =>  $post['is_default'] ?? 0,
                'is_certificates'    =>  0,
                'is_realname'    =>  0,
                'create_time'   =>  time()
            ];

            $result = Db::name('user_guest')->insert($data);

            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            return $e->getMessage();
        }

        return $result;
    }

    /**
     * 编辑用户地址
     * @param $user_id
     * @param $post
     * @return int|string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public static function editUserGuest($user_id,$post) {

        try {
            Db::startTrans();

            if (isset($post['is_default']) && $post['is_default'] == 1){
                Db::name('user_guest')
                    ->where(['del'=> 0 ,'user_id'=>$user_id])
                    ->update(['is_default'=>0]);
            }

            $age = get_age($post['idcard']);
            $sex = get_sex($post['idcard']);

            $data = [
                'name'       =>  $post['name'],
                'idcard'     =>  $post['idcard'],
                'age'       =>  $age,
                'sex'       =>  $sex,
                'is_default'    =>  $post['is_default'] ?? 0,
                'is_certificates'    =>  0,
                'is_realname'    =>  0,
                'update_time'   =>  time()
            ];

            $result = Db::name('user_guest')
                ->where(['id'=>$post['id'],'del'=> 0 ,'user_id'=>$user_id])
                ->update($data);

            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            return false;
        }

        return $result;
    }

    /**
     * 删除用户地址
     * @param $user_id
     * @param $post
     * @return int|string
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     */
    public static function delUserGuest($user_id,$post) {

        $data = [
            'del'       =>  1,
            'update_time'   =>  time()
        ];

        return  Db::name('user_guest')
            ->where(['id'=>$post['id'],'del'=>0,'user_id'=>$user_id])
            ->update($data);
    }

    /**
     * User: 意象信息科技 mjf
     * Desc: 获取用户指定id的地址
     * @param $address
     * @param $user_id
     * @return array|\PDOStatement|string|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getUserGuestById($guest, $user_id)
    {
        $info = Db::name('user_guest')
            ->where(['id' => $guest, 'user_id' => $user_id, 'del' => 0])
            ->field('id,name,idcard,sex,age,is_certificates,is_realname,is_default')
            ->find();

        if (!$info) {
            return [];
        }

        $info['sexText'] = getSexText($info['sex']);

        return $info;
    }


    //获取订单用户地址

    /**
     * User: 意象信息科技 mjf
     * Desc: 获取下单时用户地址
     * @param $data
     * @param $user_id
     * @return array|\PDOStatement|string|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getOrderUserAddress($data, $user_id)
    {
        if (isset($data['guest_id']) && $data['guest_id'] != 0){
            return self::getUserGuestById($data['guest_id'], $user_id);
        }
        return self::getDefaultGuest($user_id);
    }

}