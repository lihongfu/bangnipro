<?php
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | author:
// +----------------------------------------------------------------------


namespace app\api\controller\V1;


use app\api\controller\ApiBase;
use app\api\logic\UserGuestLogic;

class UserGuest extends ApiBase
{
    public $like_not_need_login = [];


    //获取地址列表
    public function lists()
    {
        $user_id = $this->user_id;
        $result = UserGuestLogic::infoUserGuest($user_id);
        $this->_success('获取成功', $result);
    }



    //获取一条地址详情
    public function detail()
    {
        $get = $this->request->get();
        $result = $this->validate($get, 'app\api\validate\UserGuest.one');
        if ($result === true) {
            $user_id = $this->user_id;
            $result = UserGuestLogic::getOneGuest($user_id, $get);
            if ($result) {
                $this->_success('获取成功', $result);
            }
            $result = '获取失败';
        }
        $this->_error($result);
    }


    //获取
    public function getDefault()
    {
        $user_id = $this->user_id;
        $result = UserGuestLogic::getDefaultGuest($user_id);
        $this->_success('获取成功', $result);
    }


    //设置默认地址
    public function setDefault()
    {
        $post = $this->request->post();
        $result = $this->validate($post, 'app\api\validate\UserGuest.set');
        if ($result === true) {
            $user_id = $this->user_id;
            $result = UserGuestLogic::setDefaultGuest($user_id, $post);
            if ($result) {
                $this->_success('设置成功', $result);
            }
            $result = '设置失败';
        }
        $this->_error($result);
    }



    //添加别服务人
    public function add()
    {
        $post = $this->request->post();
        $result = $this->validate($post, 'app\api\validate\UserGuest.add');
        if ($result === true) {
            $user_id = $this->user_id;

            $result = UserGuestLogic::idcardIsUnique($user_id,$post);
            if (!$result) {
                $this->_error('身份证号已存在');
            }

            $result = UserGuestLogic::addUserGuest($user_id, $post);
            if ($result) {
                $this->_success('添加成功', $result);
            }
            $result = '添加失败';
        }
        $this->_error($result);
    }


    //更新收货地址
    public function update()
    {
        $post = $this->request->post();
        $result = $this->validate($post, 'app\api\validate\UserGuest.edit');
        if ($result === true) {
            $user_id = $this->user_id;

            $result = UserGuestLogic::idcardIsUnique($user_id,$post);
            if (!$result) {
                $this->_error('身份证号已存在');
            }

            $result = UserGuestLogic::editUserGuest($user_id, $post);
            if ($result) {
                $this->_success('修改成功', $result);
            }
            $result = '修改失败';
        }
        $this->_error($result);
    }


    //删除收货地址
    public function del()
    {
        $post = $this->request->post();
        $result = $this->validate($post, 'app\api\validate\UserGuest.del');
        if ($result === true) {
            $user_id = $this->user_id;
            $result = UserGuestLogic::delUserGuest($user_id, $post);
            if ($result) {
                $this->_success('删除成功', $result);
            }
            $result = '删除失败';
        }
        $this->_error($result);
    }
}