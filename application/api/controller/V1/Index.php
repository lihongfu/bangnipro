<?php
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | author:
// +----------------------------------------------------------------------

namespace app\api\controller\V1;
use app\api\controller\ApiBase;
use app\api\logic\IndexLogic;
class Index extends ApiBase
{
   public $like_not_need_login = ['getPlatformInfo'];

    /**
     * 平台设置
     * @return mixed
     */
    public function getPlatformInfo()
    {
        $result = IndexLogic::getPlatformInfo();

        $this->_success('', $result);
    }
}