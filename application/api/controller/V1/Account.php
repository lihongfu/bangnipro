<?php
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | author:
// +----------------------------------------------------------------------


namespace app\api\controller\V1;
use app\api\controller\ApiBase;
use app\api\logic\LoginLogic;
use app\common\server\ConfigServer;

class Account extends ApiBase
{
    public $like_not_need_login = ['register','applogin', 'login', 'mnplogin', 'codeurl', 'oalogin', 'oplogin','logout','smslogin', 'uinAppLogin', 'silentLogin', 'authLogin'];

    /**
     * Notes: 小程序登录(旧系统用户,返回用户信息,否则返回空)
     * @author 段誉(2021/4/19 16:50)
     */
    public function silentLogin()
    {
        $post = $this->request->post();
        if (empty($post['code'])) {
            $this->_error('参数缺失');
        }
        $data = LoginLogic::silentLogin($post);
        $this->_success($data['msg'], $data['data'], $data['code'], $data['show']);
    }


    /**
     * Notes: 小程序登录(新用户登录->需要提交昵称和头像参数)
     * @author 段誉(2021/4/19 16:49)
     */
    public function authLogin()
    {
        $post = $this->request->post();
        if (empty($post['code']) || empty($post['nickname'])) {
            $this->_error('参数缺失');
        }
        $data = LoginLogic::authLogin($post);
        $this->_success($data['msg'], $data['data'], $data['code'], $data['show']);
    }

    /**
     * @notes 更新用户头像昵称
     * @throws \think\Exception
     * @throws \think\exception\PDOException
     * @author ljj
     * @date 2023/2/1 3:46 下午
     */
    public function updateUser()
    {
        $post = $this->request->post();
        if (!isset($post['avatar']) || empty($post['avatar'])) {
            $this->_error('参数缺失');
        }
        if (!isset($post['nickname']) || empty($post['nickname'])) {
            $this->_error('参数缺失');
        }
        $data = LoginLogic::updateUser($post,$this->user_id);
        $this->_success($data['msg'], $data['data'], $data['code'], $data['show']);
    }
}