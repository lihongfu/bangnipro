<?php
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | author: 
// +----------------------------------------------------------------------


namespace app\api\controller\V1;


use app\api\controller\ApiBase;
use app\api\logic\UserAddressLogic;

class UserAddress extends ApiBase
{
    public $like_not_need_login = ['handleregion'];


    //获取地址列表
    public function lists()
    {
        $user_id = $this->user_id;
        $result = UserAddressLogic::infoUserAddress($user_id);
        $this->_success('获取成功', $result);
    }



    //获取一条地址详情
    public function detail()
    {
        $get = $this->request->get();
        $result = $this->validate($get, 'app\api\validate\UserAddress.one');
        if ($result === true) {
            $user_id = $this->user_id;
            $result = UserAddressLogic::getOneAddress($user_id, $get);
            if ($result) {
                $this->_success('获取成功', $result);
            }
            $result = '获取失败';
        }
        $this->_error($result);
    }


    //获取默认地址
    public function getDefault()
    {
        $user_id = $this->user_id;
        $result = UserAddressLogic::getDefaultAddress($user_id);
        $this->_success('获取成功', $result);

    }


    //设置默认地址
    public function setDefault()
    {
        $post = $this->request->post();
        $result = $this->validate($post, 'app\api\validate\UserAddress.set');
        if ($result === true) {
            $user_id = $this->user_id;
            $result = UserAddressLogic::setDefaultAddress($user_id, $post);
            if ($result) {
                $this->_success('设置成功', $result);
            }
            $result = '设置失败';
        }
        $this->_error($result);
    }



    //添加收货地址
    public function add()
    {
        $post = $this->request->post();
        $result = $this->validate($post, 'app\api\validate\UserAddress.add');
        if ($result === true) {
            $user_id = $this->user_id;
            $result = UserAddressLogic::addUserAddress($user_id, $post);
            if ($result) {
                $this->_success('添加成功', $result);
            }
            $result = '添加失败';
        }
        $this->_error($result);
    }


    //更新收货地址
    public function update()
    {
        $post = $this->request->post();
        $result = $this->validate($post, 'app\api\validate\UserAddress.edit');
        if ($result === true) {
            $user_id = $this->user_id;
            $result = UserAddressLogic::editUserAddress($user_id, $post);
            if ($result) {
                $this->_success('修改成功', $result);
            }
            $result = '修改失败';
        }
        $this->_error($result);
    }


    //删除收货地址
    public function del()
    {
        $post = $this->request->post();
        $result = $this->validate($post, 'app\api\validate\UserAddress.del');
        if ($result === true) {
            $user_id = $this->user_id;
            $result = UserAddressLogic::delUserAddress($user_id, $post);
            if ($result) {
                $this->_success('删除成功', $result);
            }
            $result = '删除失败';
        }
        $this->_error($result);
    }
}