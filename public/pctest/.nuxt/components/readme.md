# Discovered Components

This is an auto-generated list of components discovered by [nuxt/components](https://github.com/nuxt/components).

You can directly use them in pages and other components without the need to import them.

**Tip:** If a component is conditionally rendered with `v-if` and is big, it is better to use `Lazy` or `lazy-` prefix to lazy load.

- `<ActivityArea>` | `<activity-area>` (components/activityArea.vue)
- `<AdItem>` | `<ad-item>` (components/adItem.vue)
- `<AddressAdd>` | `<address-add>` (components/addressAdd.vue)
- `<AddressList>` | `<address-list>` (components/addressList.vue)
- `<AfterSalesList>` | `<after-sales-list>` (components/afterSalesList.vue)
- `<CommentList>` | `<comment-list>` (components/commentList.vue)
- `<CouponsList>` | `<coupons-list>` (components/couponsList.vue)
- `<DeliverSearch>` | `<deliver-search>` (components/deliverSearch.vue)
- `<EvaluationList>` | `<evaluation-list>` (components/evaluationList.vue)
- `<GoodsList>` | `<goods-list>` (components/goodsList.vue)
- `<InputExpress>` | `<input-express>` (components/inputExpress.vue)
- `<NullData>` | `<null-data>` (components/nullData.vue)
- `<OrderList>` | `<order-list>` (components/orderList.vue)
- `<PriceFormate>` | `<price-formate>` (components/priceFormate.vue)
- `<SelffetchShopList>` | `<selffetch-shop-list>` (components/selffetchShopList.vue)
- `<UserWalletTable>` | `<user-wallet-table>` (components/userWalletTable.vue)
- `<LayoutAslideNav>` | `<layout-aslide-nav>` (components/layout/aslideNav.vue)
- `<LayoutCategory>` | `<layout-category>` (components/layout/category.vue)
- `<LayoutFloatNav>` | `<layout-float-nav>` (components/layout/floatNav.vue)
- `<LayoutFooter>` | `<layout-footer>` (components/layout/footer.vue)
- `<LayoutHeader>` | `<layout-header>` (components/layout/header.vue)
- `<PublicCountDown>` | `<public-count-down>` (components/public/countDown.vue)
- `<PublicNumberBox>` | `<public-number-box>` (components/public/numberBox.vue)
- `<PublicUpload>` | `<public-upload>` (components/public/upload.vue)
