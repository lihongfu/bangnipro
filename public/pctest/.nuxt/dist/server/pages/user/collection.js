exports.ids = [41];
exports.modules = {

/***/ 221:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(280);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("899b1c8a", content, true, context)
};

/***/ }),

/***/ 278:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/profit_null.05cb92f.png";

/***/ }),

/***/ 279:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_collection_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(221);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_collection_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_collection_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_collection_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_collection_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 280:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".user-collection{padding:10px}.user-collection .collection-header{padding:10px 5px;border-bottom:1px solid #e5e5e5}.user-collection .collection-list{display:flex;flex-direction:column;justify-content:space-between;min-height:690px}.user-collection .collection-list .goods-item{padding:26px 20px;border-bottom:1px dashed #e5e5e5}.user-collection .collection-list .goods-item .goods-info{margin-left:10px;width:500px;flex:1}.user-collection .collection-list .goods-item .goods-info .goods-name{margin-bottom:8px}.user-collection .collection-list .goods-item .to-buy-btn{border:1px solid #ff2c3c;height:32px;width:104px;font-size:13px}.user-collection .collection-list .goods-item .cancel-btn{margin-left:10px;height:32px;width:104px;border:1px solid #e5e5e5;font-size:13px;cursor:pointer}.user-collection .data-null{padding-top:100px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 326:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/user/collection.vue?vue&type=template&id=64c187b8&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"user-collection"},[_vm._ssrNode("<div class=\"collection-header lg\">\n    收藏\n  </div> "),_vm._ssrNode("<div class=\"collection-list mt10\""+(_vm._ssrStyle(null,null, { display: (_vm.goodsCount > 0) ? '' : 'none' }))+">","</div>",[_vm._ssrNode("<div>","</div>",_vm._l((_vm.collectList),function(item,index){return _vm._ssrNode("<div class=\"goods-item row-between\">","</div>",[_vm._ssrNode("<div style=\"display: flex;flex-direction: row;\">","</div>",[_c('el-image',{staticStyle:{"width":"72px","height":"72px"},attrs:{"fit":"cover","src":item.image,"lazy":""}}),_vm._ssrNode(" <div class=\"goods-info\"><div class=\"goods-name line2\">"+_vm._ssrEscape(_vm._s(item.name))+"</div> <div class=\"primary nr\" style=\"font-weight: 500;\">"+_vm._ssrEscape("¥"+_vm._s(item.price))+"</div></div>")],2),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"row\">","</div>",[_c('nuxt-link',{staticClass:"to-buy-btn primary row-center",attrs:{"to":'/goods_details/'+item.id}},[_vm._v("去购买")]),_vm._ssrNode(" "),_c('el-popconfirm',{attrs:{"title":"确定删除该收藏吗？","confirm-button-text":"确定","cancel-button-text":"取消","icon":"el-icon-info","icon-color":"red"},on:{"confirm":function($event){return _vm.cancelCollection(item.id)}}},[_c('div',{staticClass:"cancel-btn lighter row-center",attrs:{"slot":"reference"},slot:"reference"},[_vm._v("取消收藏")])])],2)],2)}),0),_vm._ssrNode(" "),_vm._ssrNode("<div class=\"row-center\" style=\"margin-top: 30px\">","</div>",[_c('el-pagination',{attrs:{"hide-on-single-page":"","background":"","layout":"prev, pager, next","total":_vm.goodsCount,"prev-text":"上一页","next-text":"下一页","page-size":_vm.pageSize},on:{"current-change":_vm.changePage}})],1)],2),_vm._ssrNode(" <div class=\"column-center data-null\""+(_vm._ssrStyle(null,null, { display: (_vm.goodsCount <= 0) ? '' : 'none' }))+"><img"+(_vm._ssrAttr("src",__webpack_require__(278)))+" style=\"width: 150px;height: 150px;\"> <div class=\"muted xs\">暂无收藏商品～</div></div>")],2)}
var staticRenderFns = []


// CONCATENATED MODULE: ./pages/user/collection.vue?vue&type=template&id=64c187b8&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./pages/user/collection.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var collectionvue_type_script_lang_js_ = ({
  head() {
    return {
      title: this.$store.getters.headTitle,
      link: [{
        rel: "icon",
        type: "image/x-icon",
        href: this.$store.getters.favicon
      }]
    };
  },

  layout: "user_lauout",

  async asyncData({
    $post,
    $get
  }) {
    let collectList = [];
    let goodsCount = 0;
    let pageSize = 5;
    let res = await $get("collect/getCollectGoods", {
      params: {
        page_no: 1,
        page_size: pageSize
      }
    });

    if (res.code == 1) {
      collectList = res.data.list;
      goodsCount = res.data.count;
    }

    return {
      collectList,
      goodsCount,
      pageSize
    };
  },

  data() {
    return {
      currentPage: 1
    };
  },

  methods: {
    async changePage(currentPage) {
      this.currentPage = currentPage;
      let res = await this.$get("collect/getCollectGoods", {
        params: {
          page_no: currentPage,
          page_size: this.pageSize
        }
      });

      if (res.code == 1) {
        this.collectList = res.data.list;
        this.goodsCount = res.data.count;
      }
    },

    async cancelCollection(id) {
      let res = await this.$post('collect/handleCollectGoods', {
        is_collect: 0,
        goods_id: id
      });

      if (res.code == 1) {
        this.$message({
          message: '已取消收藏',
          type: 'success'
        });
        this.changePage(this.currentPage);
      }
    }

  }
});
// CONCATENATED MODULE: ./pages/user/collection.vue?vue&type=script&lang=js&
 /* harmony default export */ var user_collectionvue_type_script_lang_js_ = (collectionvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1);

// CONCATENATED MODULE: ./pages/user/collection.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(279)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  user_collectionvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  null,
  "07d323d6"
  
)

/* harmony default export */ var collection = __webpack_exports__["default"] = (component.exports);

/***/ })

};;
//# sourceMappingURL=collection.js.map