exports.ids = [6,12];
exports.modules = {

/***/ 134:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(137);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("7277513c", content, true, context)
};

/***/ }),

/***/ 136:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_nullData_vue_vue_type_style_index_0_id_7fa0e58c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(134);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_nullData_vue_vue_type_style_index_0_id_7fa0e58c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_nullData_vue_vue_type_style_index_0_id_7fa0e58c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_nullData_vue_vue_type_style_index_0_id_7fa0e58c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_nullData_vue_vue_type_style_index_0_id_7fa0e58c_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 137:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".null-data[data-v-7fa0e58c]{padding:100px}.null-data .img-null[data-v-7fa0e58c]{width:150px;height:150px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/nullData.vue?vue&type=template&id=7fa0e58c&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"bg-white column-center null-data"},[_vm._ssrNode("<img"+(_vm._ssrAttr("src",_vm.img))+" alt class=\"img-null\""+(_vm._ssrStyle(null,_vm.imgStyle, null))+" data-v-7fa0e58c> <div class=\"muted mt8\" data-v-7fa0e58c>"+_vm._ssrEscape(_vm._s(_vm.text))+"</div>")])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/nullData.vue?vue&type=template&id=7fa0e58c&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/nullData.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
/* harmony default export */ var nullDatavue_type_script_lang_js_ = ({
  components: {},
  props: {
    img: {
      type: String
    },
    text: {
      type: String,
      default: "暂无数据"
    },
    imgStyle: {
      type: String,
      default: ''
    }
  },
  methods: {}
});
// CONCATENATED MODULE: ./components/nullData.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_nullDatavue_type_script_lang_js_ = (nullDatavue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1);

// CONCATENATED MODULE: ./components/nullData.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(136)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_nullDatavue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "7fa0e58c",
  "5933b7a8"
  
)

/* harmony default export */ var nullData = __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ 157:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "img/news_null.856b3f3.png";

/***/ }),

/***/ 182:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(201);
if(content.__esModule) content = content.default;
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add CSS to SSR context
var add = __webpack_require__(4).default
module.exports.__inject__ = function (context) {
  add("75275508", content, true, context)
};

/***/ }),

/***/ 200:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_commentList_vue_vue_type_style_index_0_id_780b4851_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(182);
/* harmony import */ var _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_commentList_vue_vue_type_style_index_0_id_780b4851_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_commentList_vue_vue_type_style_index_0_id_780b4851_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_commentList_vue_vue_type_style_index_0_id_780b4851_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_vue_style_loader_index_js_ref_7_oneOf_1_0_node_modules_css_loader_dist_cjs_js_ref_7_oneOf_1_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_oneOf_1_2_node_modules_sass_loader_dist_cjs_js_ref_7_oneOf_1_3_node_modules_sass_resources_loader_lib_loader_js_ref_7_oneOf_1_4_node_modules_nuxt_components_dist_loader_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_commentList_vue_vue_type_style_index_0_id_780b4851_lang_scss_scoped_true___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ 201:
/***/ (function(module, exports, __webpack_require__) {

// Imports
var ___CSS_LOADER_API_IMPORT___ = __webpack_require__(3);
var ___CSS_LOADER_EXPORT___ = ___CSS_LOADER_API_IMPORT___(false);
// Module
___CSS_LOADER_EXPORT___.push([module.i, ".comment-list .comment-con>.item[data-v-780b4851]{padding:20px;border-bottom:1px dashed #e5e5e5;align-items:flex-start}.comment-list .comment-con>.item .avatar img[data-v-780b4851]{border-radius:50%;width:44px;height:44px}.comment-list .comment-con>.item .comment-imglist[data-v-780b4851]{margin-top:10px}.comment-list .comment-con>.item .comment-imglist .item[data-v-780b4851]{width:80px;height:80px;margin-right:6px}.comment-list .comment-con>.item .reply[data-v-780b4851]{background-color:#f2f2f2;align-items:flex-start;padding:10px}", ""]);
// Exports
module.exports = ___CSS_LOADER_EXPORT___;


/***/ }),

/***/ 239:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
// ESM COMPAT FLAG
__webpack_require__.r(__webpack_exports__);

// CONCATENATED MODULE: ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/commentList.vue?vue&type=template&id=780b4851&scoped=true&
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"comment-list"},[_vm._ssrNode("<div class=\"comment-con\" data-v-780b4851>","</div>",[(_vm.commentList.length)?[_vm._l((_vm.commentList),function(item,index){return _vm._ssrNode("<div class=\"item row\" data-v-780b4851>","</div>",[_vm._ssrNode("<div class=\"avatar mr8\" data-v-780b4851><img"+(_vm._ssrAttr("src",item.avatar))+" alt data-v-780b4851></div> "),_vm._ssrNode("<div class=\"content flex1\" data-v-780b4851>","</div>",[_vm._ssrNode("<div data-v-780b4851>"+_vm._ssrEscape(_vm._s(item.nickname))+"</div> <div class=\"lighter\" style=\"margin: 5px 0 10px\" data-v-780b4851><span data-v-780b4851>"+_vm._ssrEscape(_vm._s(item.create_time))+"</span> <span data-v-780b4851>|</span> <span data-v-780b4851>"+_vm._ssrEscape("规格："+_vm._s(item.spec_value_str))+"</span></div> <div data-v-780b4851>"+_vm._ssrEscape("\n            "+_vm._s(item.comment)+"\n          ")+"</div> "),_vm._ssrNode("<div class=\"comment-imglist row\" data-v-780b4851>","</div>",_vm._l((item.image),function(img,index){return _vm._ssrNode("<div class=\"item\" data-v-780b4851>","</div>",[_c('el-image',{staticStyle:{"height":"100%","width":"100%"},attrs:{"preview-src-list":item.image,"src":img,"fit":"contain"}})],1)}),0),_vm._ssrNode(" "+((item.reply)?("<div class=\"row reply mt16\" data-v-780b4851><div class=\"primary flexnone\" data-v-780b4851>商家回复：</div> <div class=\"lighter\" data-v-780b4851>"+_vm._ssrEscape("\n              "+_vm._s(item.reply)+"\n            ")+"</div></div>"):"<!---->"))],2)],2)}),_vm._ssrNode(" "),(_vm.count)?_vm._ssrNode("<div class=\"pagination row-center\" style=\"padding: 38px 0\" data-v-780b4851>","</div>",[_c('el-pagination',{attrs:{"background":"","hide-on-single-page":"","layout":"prev, pager, next","total":_vm.count,"prev-text":"上一页","next-text":"下一页","page-size":10},on:{"current-change":_vm.changePage}})],1):_vm._e()]:_c('null-data',{attrs:{"img":__webpack_require__(157),"text":"暂无评价~"}})],2)])}
var staticRenderFns = []


// CONCATENATED MODULE: ./components/commentList.vue?vue&type=template&id=780b4851&scoped=true&

// CONCATENATED MODULE: ./node_modules/babel-loader/lib??ref--2-0!./node_modules/@nuxt/components/dist/loader.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./components/commentList.vue?vue&type=script&lang=js&
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ var commentListvue_type_script_lang_js_ = ({
  components: {},
  props: {
    list: {
      type: Array,
      default: () => []
    },
    type: Number,
    goodsId: [String, Number]
  },

  data() {
    return {
      commentList: [],
      count: 0,
      page: 1
    };
  },

  created() {
    this.getCommentList();
  },

  methods: {
    async getCommentList() {
      const {
        data,
        code
      } = await this.$get("goods_comment/lists", {
        params: {
          id: this.type,
          goods_id: this.goodsId,
          page_size: 10,
          page_no: this.page
        }
      });

      if (code == 1) {
        this.commentList = data.list;
        this.count = data.count;
      }
    },

    changePage(current) {
      this.page = current;
      this.getCommentList();
    }

  }
});
// CONCATENATED MODULE: ./components/commentList.vue?vue&type=script&lang=js&
 /* harmony default export */ var components_commentListvue_type_script_lang_js_ = (commentListvue_type_script_lang_js_); 
// EXTERNAL MODULE: ./node_modules/vue-loader/lib/runtime/componentNormalizer.js
var componentNormalizer = __webpack_require__(1);

// CONCATENATED MODULE: ./components/commentList.vue



function injectStyles (context) {
  
  var style0 = __webpack_require__(200)
if (style0.__inject__) style0.__inject__(context)

}

/* normalize component */

var component = Object(componentNormalizer["a" /* default */])(
  components_commentListvue_type_script_lang_js_,
  render,
  staticRenderFns,
  false,
  injectStyles,
  "780b4851",
  "d0f2d8d4"
  
)

/* harmony default export */ var commentList = __webpack_exports__["default"] = (component.exports);

/* nuxt-component-imports */
installComponents(component, {NullData: __webpack_require__(139).default})


/***/ })

};;
//# sourceMappingURL=comment-list.js.map